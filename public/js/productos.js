//LA API FETCH PROPORCIONA UNA INTERFAZ PARA RECUPERAR RECURSOS (INCLUSO, A TRAVÉS DE LA RED)
let traeProductos = () => {
    fetch('/productos/')
        .then(response => response.json())
        .then(data => armaTemplate(data));

    function armaTemplate(productos) {
        let template = ""
        //A CONTINUACIÓN AGREGAMOS EL PRECIO EN EL TEMPLATE
        productos.forEach(producto => {
            let precioFormateado = new Intl.NumberFormat('es-AR').format(producto.precio);
            template +=
                `<article>
                    <h3 class="descripcion">${producto.descripcion}</h3>
                    <img src="${producto.imagen}" class="imagen">
                    <p class="precio">Precio: ${precioFormateado}</p>
                </article>`
        });
        document.querySelector("#producto").innerHTML = template;
    }
}

traeProductos();