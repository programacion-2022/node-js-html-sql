//https://smarts.com.ar/
let productos = [
    {"id":1, "descripcion":"Smart Watch", "imagen":"https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/t/i/ticwatch-e3.gif"},
    {"id":2, "descripcion":"Notebook Asus", "imagen": "https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/9/0/90nb0ty1-m00c20-12gb---foto-1-con-caracteristicas_1.jpg"},
    {"id":3, "descripcion":"impresora HP Ink tank 415", "imagen": "https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/6/1/6199_1.jpg"},
]
//AQUÍ CARGAMOS MYSQL---------------------------------
let mysql = require("mysql");
//SI NO RECONOCE MYSQL, LE FALTARÁ INSTALARLO, ES DECIR npm i mysql EN LA TERMINAL

//CREAMOS LA CONEXIÓN A MYSQL, EN ESTA CASO CON LA BASE DE DATOS
let connection = mysql.createConnection({
    host: 'localhost',
    user: 'Carlos',
    password: 'carlos',
    database: 'tecnoshop',
})

//CONECTAMOS A LA BASE DE DATOS
connection.connect((err) => {
    //INTENTAMOS LA CONEXIÓN
    if (err) throw err; //SI EXISTE ERROR, LANZAMOS EL MISMO
    console.log("Se conectó correctamente");
});

//LEEMOS TODOS LOS PRODUCTOS
connection.query("SELECT * FROM productos", (err, rows) => {
    if (err) throw err;
    console.log("Los datos de la tabla users son: ");
    console.log(rows);
    console.log(rows.length);
    console.log(rows[0]);
    console.log(rows[0].descripcion);
    const productoUno = rows[0];
    console.log(productoUno.descripcion);
    ////////////////////////////////////////////////////////////
    //ASIGNAMOS LOS PRODUCTOS DE LA ENTIDAD DE LA BD, AL ARRAY//
    ////////////////////////////////////////////////////////////
    productos = rows;
});
//connection.end();               //CERRAMOS LA CONEXIÓN

const express = require("express");
const app = express();
app.use(express.json());

const path = require('path');

app.use(express.static('public'));
app.use('/public', express.static('public'));

app.get('/', (req, res) => {
    res.send('<h1>BIENVENIDOS!</h1>');
});

app.get('/productos/', (req, res) =>{
    res.json(productos);
});

app.get('/productos/:id', (req, res) =>{
    const id = Number(req.params.id);
    // res.send(id);
    const producto = productos.find(produ => produ.id === id);
    // console.log(producto);
    res.json(producto);
    
});

app.delete('/productos/:id', (req, res) => {
    const id = Number(req.params.id);
    // res.send(`Borrando el id: ${id}`);
    const producto = productos.find(produ => produ.id === id);
    console.log(producto);
    productos = productos.filter(produ => produ.id !== id);
    res.json(productos);
})

app.post('/productos/', (req, res) => {
    const producto = req.body;
    // res.send('Ingresamos al post');
    //{"id":4, "contain":"Monitor Gamer Nacional 24 pulagadas', "imagen": "https://smarts.com.ar/media/catalog/product/cache/5b8653d9435ca7b8c035b3971b386dd0/g/5/g50s.gif"}
    console.log(producto);
    res.json(producto);
    productos.push(producto);
})

const port = 3001;
app.listen(port);
console.log(`Server is running on port ${port}`);
